-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Сен 17 2017 г., 19:32
-- Версия сервера: 5.7.15
-- Версия PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dev.oblcenter2_2`
--

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `html` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `banner`
--

INSERT INTO `banner` (`id`, `alias`, `name`, `html`) VALUES
(1, 'address', 'Адрес', 'ЧЕЛЯБИНСК,<br>ЭНТУЗИАСТОВ 28А,<br>ОФИС 601'),
(2, 'email1', 'Email1', 'oblcentr74@gmail.com'),
(3, 'email2', 'Email2', '2232260@mail.ru'),
(4, 'phone', 'Телефон', '+7 (351) 223-22-60'),
(5, 'address_line', 'Адрес в строку', 'ЧЕЛЯБИНСК, ЭНТУЗИАСТОВ 28А, ОФИС 601');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_section`
--

CREATE TABLE `catalog_section` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) DEFAULT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position_table` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `section_table` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `children_tpl` text COLLATE utf8_unicode_ci,
  `leaf` smallint(6) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `catalog_section`
--

INSERT INTO `catalog_section` (`id`, `parent_id`, `title`, `position`, `alias`, `position_table`, `section_table`, `children_tpl`, `leaf`) VALUES
(1, NULL, 'Формы', 1, 'forms_section', NULL, 'section_forms_settings', '<tpl>\n  <section>section_forms</section>\n  <position>position_forms</position>\n  <leaf>1</leaf>\n</tpl>', NULL),
(2, NULL, 'Почему мы', 3, 'pochemu-my', NULL, NULL, NULL, NULL),
(3, 2, '100%', 1, 'komanda', '', 'section_page', '', NULL),
(4, 2, '15 млн', 3, 'kompitenciya', '', 'section_page', '', NULL),
(5, 2, 'Опыт', 4, 'opyt', '', 'section_page', '', NULL),
(6, 2, '8 млн', 2, 'otvetstvennost', '', 'section_page', '', NULL),
(7, NULL, 'Часто задаваемые вопросы', 4, 'chasto-zadavaemye-voprosy', '', 'section_page', '', NULL),
(8, 7, 'Допустим, в моей квартире есть строительные недостатки. Как я могу вернуть деньги по ДДУ?', 1, 'iz-kadastrovoj-stoimosti-zemli-schitaetsya-zemelnyj-nalog-verno', '', 'section_page', '', NULL),
(9, 7, 'О каких строительных недостатках идет речь?', 2, 'vse-govoryat-chto-kadastrovaya-stoimost-zavyshena-chto-s-nej-ne-tak', '', 'section_page', '', NULL),
(10, 7, 'Получается, по любому договору долевого участия можно получить компенсацию?', 3, 'skazhem-u-menya-poluchitsya-osporit-kadastrovuyu-stoimost-a-na-kakoj-period-eto-budet-rasprostranyatsya', '', 'section_page', '', NULL),
(35, 7, 'Чем вы еще можете помочь дольщикам?', 6, 'chem-vy-ecshe-mozhete-pomoch-dolcshikam', '', 'section_page', '', NULL),
(34, 7, 'При техническом осмотре не пострадает отделка моей квартиры?', 5, 'pri-tehnicheskom-osmotre-ne-postradaet-otdelka-moej-kvartiry', NULL, NULL, NULL, NULL),
(12, NULL, 'Нам доверяют', 5, 'nam-doveryayut', NULL, NULL, NULL, NULL),
(13, 12, 'Фабрика овощей', 1, 'fabrika-ovocshej', '', 'section_page', '', NULL),
(14, 12, 'Миасский керамический завод', 2, 'miasskij-keramicheskij-zavod', '', 'section_page', '', NULL),
(15, 12, 'Полиграфическое объединение Книга', 3, 'poligraficheskoe-obedinenie-kniga', '', 'section_page', '', NULL),
(16, 12, 'Троицкий электромеханический завод', 4, 'troickij-elektromehanicheskij-zavod', '', 'section_page', '', NULL),
(17, 12, 'Челябкрансервис', 5, 'chelyabkranservis', '', 'section_page', '', NULL),
(18, 12, 'Акционерное общество ЭНСЕР', 6, 'akcionernoe-obcshestvo-enser', '', 'section_page', '', NULL),
(19, 12, 'Анкер', 7, 'anker', '', 'section_page', '', NULL),
(20, 12, 'Мечел', 8, 'mechel', '', 'section_page', '', NULL),
(21, NULL, 'Как происходит сотрудничество', 6, 'kak-proishodit-sotrudnichestvo', NULL, NULL, NULL, NULL),
(22, 21, '1', 1, '1', '', 'section_page', '', NULL),
(23, 21, '2', 2, '2', '', 'section_page', '', NULL),
(24, 21, '3', 3, '3', '', 'section_page', '', NULL),
(25, 21, '4', 4, '4', '', 'section_page', '', NULL),
(26, 21, '5', 5, '5', '', 'section_page', '', NULL),
(27, 1, 'Заявка', 1, 'zayavka', 'position_forms', 'section_forms', NULL, 1),
(28, 1, 'Связаться с нами', 2, 'svyazatsya_s_nami', 'position_forms', 'section_forms', '', 1),
(29, 1, 'ЗАДАТЬ ВОПРОС СПЕЦИАЛИСТУ', 3, 'zadat_vopros_specialistu', 'position_forms', 'section_forms', '', 1),
(30, NULL, 'ОСПОРИТЬ КАДАСТРОВУЮ СТОИМОСТЬ НЕДВИЖИМОСТИ', 2, 'osporit-kadastrovuyu-stoimost-nedvizhimosti', '', 'section_page', '', NULL),
(33, 7, 'Можно ли сделать предварительный осмотр моей квартиры? ', 4, 'mozhno-li-sdelat-predvaritelnyj-osmotr-moej-kvartiry', '', 'section_page', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `feed`
--

CREATE TABLE `feed` (
  `id` int(11) NOT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interval_update` int(11) DEFAULT NULL,
  `datestamp_update` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `feed_item`
--

CREATE TABLE `feed_item` (
  `id` int(11) NOT NULL,
  `feed_id` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `link` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pubdate` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `forms_field_action`
--

CREATE TABLE `forms_field_action` (
  `id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `position` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `forms_field_action`
--

INSERT INTO `forms_field_action` (`id`, `name`, `position`) VALUES
('auth', 'Авторизация', 2),
('is_active', 'Активация учетной запись', 4),
('register', 'Регистрация', 1),
('restore_pass', 'Восстановить пароль', 3),
('send_email', 'Отправка на email', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `forms_field_type`
--

CREATE TABLE `forms_field_type` (
  `id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `position` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `forms_field_type`
--

INSERT INTO `forms_field_type` (`id`, `name`, `position`) VALUES
('check', 'Флаг', 7),
('date', 'Дата', 5),
('hidden', 'Скрытое поле', 11),
('html', 'Произвольный HTML', 15),
('info', 'Информация', 14),
('label', 'Подпись', 13),
('link', 'Ссылка', 10),
('memo', 'Многострочный текст', 2),
('pwd', 'Пароль', 6),
('radiobox', 'Радиобокс', 12),
('select', 'Выпадающий список', 3),
('submit', 'Кнопка подачи запроса', 8),
('text', 'Текстовая строка', 1),
('upload', 'Обзор', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `infoblock`
--

CREATE TABLE `infoblock` (
  `id` int(11) NOT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `html` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `title`) VALUES
(1, 'main', 'Главное меню');

-- --------------------------------------------------------

--
-- Структура таблицы `menus_item`
--

CREATE TABLE `menus_item` (
  `id` int(11) NOT NULL,
  `menus_id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `visible` smallint(6) NOT NULL DEFAULT '0',
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `type_link` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attr` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_src` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `menus_item`
--

INSERT INTO `menus_item` (`id`, `menus_id`, `title`, `parent_id`, `position`, `visible`, `type`, `type_id`, `type_link`, `attr`, `img_src`) VALUES
(1, 1, 'Почему мы', NULL, 1, 1, '', NULL, '', '#wrap2', NULL),
(2, 1, 'Нам доверяют', NULL, 2, 1, '', NULL, '', '#jdun', NULL),
(3, 1, 'Этапы работ', NULL, 3, 1, '', NULL, '', '#coop', NULL),
(4, 1, 'Заполните заявку', NULL, 4, 1, '', NULL, '', '#anketa', NULL),
(5, 1, 'Каталог', NULL, 5, 1, '', NULL, '', '#questions', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title2` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `position` int(11) NOT NULL,
  `visible` smallint(6) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `alias` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plugins` text COLLATE utf8_unicode_ci,
  `mainpage` smallint(6) NOT NULL DEFAULT '0',
  `announcement` text COLLATE utf8_unicode_ci,
  `fileupload` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_src` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect` smallint(6) DEFAULT NULL,
  `head_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `tag` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `title`, `title2`, `content`, `position`, `visible`, `parent_id`, `alias`, `template`, `plugins`, `mainpage`, `announcement`, `fileupload`, `img_src`, `redirect`, `head_title`, `meta_keywords`, `meta_description`, `tag`) VALUES
(1, 'Главная', '', '			<h2>Оспорить кадастровую стоимость недвижимости</h2>\n			<h3>Гарантия обеспечения стопроцентного результата!</h3>	', 1, 0, NULL, 'index', 'offer_page.tpl.php', '', 1, '', NULL, NULL, NULL, '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `position_forms`
--

CREATE TABLE `position_forms` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `type_id` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_empty` smallint(6) DEFAULT '0',
  `valid_email` smallint(6) DEFAULT '0',
  `select_options` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `nameid` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `html_before` text,
  `html_after` text,
  `retailcrm_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_forms`
--

INSERT INTO `position_forms` (`id`, `section_id`, `name`, `position`, `type_id`, `valid_empty`, `valid_email`, `select_options`, `nameid`, `html_before`, `html_after`, `retailcrm_name`) VALUES
(1, 27, 'В каком районе находится ваша квартира', 10, 'select', 1, NULL, 'Выбор района\nВыбор района\nВыбор района', 'rayon', '<div class="col-xs-12 col-md-6">\n   <div class="form-group">', NULL, NULL),
(2, 27, 'Улица', 20, 'text', 1, NULL, NULL, 'ulica', NULL, NULL, 'name'),
(3, 27, 'Сколько комнат в вашей квартире', 30, 'select', 1, NULL, 'Студия\n2х-комнатная\n3х-комнатная\n4х-комнатная', 'kvartiri', NULL, '\n', 'phone'),
(4, 27, 'Делали ли вы ремонт?', 40, 'select', NULL, 1, 'Да\nНет (отделка застройщика)\nНет (квартира без отделки)\nРемонт в некоторых помещениях', 'remont', NULL, NULL, 'email'),
(5, 27, 'Какие недостатки вас беспокоят?', 60, 'select', NULL, NULL, 'Неровность стен\nНеровность потолка\nНеровность пола\nШелушение/разрушение покрытия потолка\nТрещины между плитами перекрытия\nНекачественная поклейка обоев\nХолодно в квартире\nПротекание плит перекрытий\nДругое', 'nedostatki', NULL, '</div>\n</div>\n<div class="col-xs-12 col-md-6">\n   <div class="form-group">', 'mess'),
(6, 27, 'Отправить', 110, 'submit', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 28, 'Пожалуйста представьтесь', 10, 'text', 1, NULL, NULL, 'name', '<div class="form-group">', NULL, 'name'),
(8, 28, 'Телефон', 20, 'text', 1, NULL, NULL, 'phone', NULL, NULL, 'phone'),
(9, 28, 'e-mail', 30, 'text', NULL, 1, NULL, 'email', NULL, NULL, 'email'),
(10, 28, 'Текст сообщения', 40, 'memo', NULL, NULL, NULL, 'comm', NULL, '</div>', 'comm'),
(11, 28, 'Отправить', 60, 'submit', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 29, 'Пожалуйста представьтесь', 10, 'text', 1, NULL, NULL, 'name', '<div class="form-group">', NULL, 'name'),
(13, 29, 'Телефон', 20, 'text', 1, NULL, NULL, 'phone', NULL, NULL, 'phone'),
(14, 29, 'e-mail', 30, 'text', NULL, 1, NULL, 'email', NULL, NULL, 'email'),
(15, 29, 'Текст сообщения', 40, 'memo', NULL, NULL, NULL, 'comm', NULL, NULL, 'comm'),
(16, 29, 'Отправить', 60, 'submit', NULL, NULL, NULL, NULL, NULL, '</form>', NULL),
(17, 27, 'Как мы можем к вам обращаться', 70, 'text', NULL, NULL, NULL, 'name', NULL, NULL, NULL),
(18, 27, 'Ваша электронная почта', 80, 'text', NULL, NULL, NULL, 'email', NULL, NULL, NULL),
(19, 27, 'Ваш контактный номер', 90, 'text', NULL, NULL, NULL, 'тгьиук', NULL, '</div>\n</div>\n<div class="col-xs-12 col-xs-12">\n   <div class="form-group2">', NULL),
(20, 27, 'Здесь вы можете прикрепить фото строительных недостатков', 100, 'upload', NULL, NULL, NULL, 'loops', '<label>', '<span>Здесь вы можете прикрепить фото<br> строительных недостатков</span>\n</div>\n</label>', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `position_news`
--

CREATE TABLE `position_news` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datestamp` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` longtext COLLATE utf8_unicode_ci,
  `img` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public` tinyint(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `position_setting`
--

CREATE TABLE `position_setting` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `img` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `public` tinyint(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `redirection`
--

CREATE TABLE `redirection` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `img_src` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `section_forms`
--

CREATE TABLE `section_forms` (
  `id` int(11) NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efrom` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efromname` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `esubject` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `captcha` smallint(6) DEFAULT '0',
  `header_mail` text COLLATE utf8_unicode_ci,
  `title_form` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `success_message` text COLLATE utf8_unicode_ci,
  `html` text COLLATE utf8_unicode_ci,
  `html_id` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_form` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'post',
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `save_table` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_order_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_on` tinyint(2) DEFAULT NULL,
  `ya_metrika_target_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_target_id_button` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_on` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `section_forms`
--

INSERT INTO `section_forms` (`id`, `email`, `efrom`, `efromname`, `esubject`, `captcha`, `header_mail`, `title_form`, `success_message`, `html`, `html_id`, `class_form`, `method`, `action`, `save_table`, `retailcrm_order_method`, `retailcrm_on`, `ya_metrika_target_id`, `ya_metrika_target_id_button`, `ya_metrika_on`) VALUES
(27, 'cefar@mail.ru', NULL, NULL, 'Заявка с сайта > oblcenter', 0, NULL, NULL, 'Спасибо за обращение!', '<p>{KADASTRONIY_NOMER}</p>\n\n<p>{NAME}</p>\n\n<p>{PHONE}</p>\n\n<p>{EMAIL}</p>\n\n<p>{COMM}</p>\n', 'form_zayavka', NULL, 'ajax-post', 'send_email', NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'cefar@mail.ru', NULL, NULL, 'Связываются с сайта > oblcenter', 0, NULL, NULL, 'Спасибо за обращение!', '<p>{NAME}</p>\n\n<p>{PHONE}</p>\n\n<p>{EMAIL}</p>\n\n<p>{COMM}</p>\n', 'form_svyazatsya_s_nami', NULL, 'ajax-post', 'send_email', NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'cefar@mail.ru', NULL, NULL, 'Вопрос специалисту > oblcenter', 0, NULL, NULL, 'Спасибо за обращение!', '<p>{NAME}</p>\n\n<p>{PHONE}</p>\n\n<p>{EMAIL}</p>\n\n<p>{COMM}</p>\n', 'zadat_vopros_form', NULL, 'ajax-post', 'send_email', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `section_forms_settings`
--

CREATE TABLE `section_forms_settings` (
  `id` int(11) NOT NULL,
  `ya_metrika_key` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_on` tinyint(2) DEFAULT NULL,
  `retailcrm_key` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_on` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `section_forms_settings`
--

INSERT INTO `section_forms_settings` (`id`, `ya_metrika_key`, `ya_metrika_id`, `ya_metrika_on`, `retailcrm_key`, `retailcrm_on`) VALUES
(1, NULL, NULL, NULL, NULL, NULL),
(3, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `section_form_feedback`
--

CREATE TABLE `section_form_feedback` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efrom` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efromname` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `esubject` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `section_news`
--

CREATE TABLE `section_news` (
  `id` int(11) NOT NULL,
  `page_size` int(11) DEFAULT NULL,
  `head_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `tag` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `section_news`
--

INSERT INTO `section_news` (`id`, `page_size`, `head_title`, `meta_keywords`, `meta_description`, `tag`) VALUES
(3, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `section_page`
--

CREATE TABLE `section_page` (
  `id` int(11) NOT NULL,
  `small_text` text COLLATE utf8_unicode_ci,
  `content` longtext COLLATE utf8_unicode_ci,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visible` smallint(6) DEFAULT '1',
  `head_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `tag` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `section_page`
--

INSERT INTO `section_page` (`id`, `small_text`, `content`, `img`, `visible`, `head_title`, `meta_keywords`, `meta_description`, `tag`) VALUES
(3, '<p>Случаев, когда мы доказали обоснованность своих претензий к застройщику.</p>\n', NULL, 'files/catalog/upload/28e3c4d574ecedd839fa8f3890d58601.png', 1, NULL, NULL, NULL, NULL),
(4, '<p>Мы вернули дольщикам за строительный брак</p>\n', NULL, 'files/catalog/upload/272f539b3a52a5e2da17dea1d8a73f03.png', 1, NULL, NULL, NULL, NULL),
(5, '<p>Мы участвовали в формировании судебной и правоприминительной практики по ряду услуг которые предлагаем.</p>\n', NULL, 'files/catalog/upload/de6f2aef327d84cbc42f97e0d79995b2.png', 1, NULL, NULL, NULL, NULL),
(6, '<p>Неустойки за нарушение сроков сдачи жилья получили наши клиенты</p>\n', NULL, 'files/catalog/upload/90cf0a54b81d6d950c19718808fadad4.png', 1, NULL, NULL, NULL, NULL),
(8, 'Допустим, в моей квартире есть строительные недостатки. Как я могу вернуть деньги по ДДУ?\n', '<p>- Это ваше право по закону. Посмотрите внимательно свой договор долевого участия, в нем обязательно содержится условие о гарантийном сроке на квартиру. Обычно это 3-5 лет. Застройщик обязан обеспечить гарантийный срок на строительно-монтажные и отделочные работы в квартире. Если мы сумеем выявить здесь недостатки и правильно их обоснуем, у нас появится право требовать соразмерного уменьшения цены ДДУ.</p>\n', NULL, 1, NULL, NULL, NULL, NULL),
(9, 'О каких строительных недостатках идет речь?', '<p>Иногда наши клиенты обращаются к нам с явными претензиями: в квартире течет потолок, в плитах и перекрытиях &ndash; трещины&hellip; Другие недостатки могут выявить только наши специалисты в рамках технического обследования. Не все собственники знают, например, о том, какие требования предъявляются к маркировке стеклопакетов или отделке помещений. Оставьте <a href="#anketa" class="yak">заявку</a> на нашем сайте, и мы сможем сориентировать вас, о какой сумме может идти речь в вашем случае.&nbsp;</p>\n', NULL, 1, NULL, NULL, NULL, NULL),
(10, 'Получается, по любому договору долевого участия можно получить компенсацию?', '<p>Это далеко не так. В вашей квартире может быть все в порядке. Возможно, какие-то строительные недостатки и есть, но их восстановительная стоимость незначительна. В конце концов, вы можете сделать в квартире ремонт, и тогда выявить что-либо будет очень трудно.&nbsp;</p>\n', NULL, 1, NULL, NULL, NULL, NULL),
(7, 'При техническом осмотре не пострадает отделка моей квартиры?', '<p>Не беспокойтесь, не пострадает. Мы не снимаем напольные покрытия, не нарушаем целостность монтажных или отделочных работ.&nbsp;</p>\n', NULL, 1, NULL, NULL, NULL, NULL),
(35, 'Чем вы еще можете помочь дольщикам?', '<p>Наша компания в течение последний лет оказывает услуги по защите прав дольщиков. Взыскание неустойки с застройщика при пропуске срока сдачи жилья, расторжение договора долевого участия, участие в процедурах банкротства &ndash; во всех этих вопросах вы можете на нас положиться. Свяжитесь с нашими юристами или &lt;a href=&quot;anketa&quot; class=&quot;yak&quot;&gt;оставьте свои координаты для обратной связи.&lt;/a&gt;&nbsp;Мы поможем вам.</p>\n', NULL, 1, NULL, NULL, NULL, NULL),
(33, 'Можно ли сделать предварительный осмотр моей квартиры? ', '<p>Да, и это бесплатная услуга для Челябинска. <a href="#anketa" class="yak">Оставьте заявку</a> на выезд нашего специалиста. Мы произведем осмотр вашей квартиры и сориентируем вас, насколько возможно снижение цены вашего договора ДДУ.</p>\n', NULL, 1, NULL, NULL, NULL, NULL),
(13, NULL, NULL, 'files/catalog/upload/a2505a07ac73c5d5a081a4944543196b.png', 1, NULL, NULL, NULL, NULL),
(14, NULL, NULL, 'files/catalog/upload/0c3b015588f541cb6f26b6a4904ead9e.jpg', 1, NULL, NULL, NULL, NULL),
(15, NULL, NULL, 'files/catalog/upload/63ab8462a5b196e641939661f528aba8.jpg', 1, NULL, NULL, NULL, NULL),
(16, NULL, NULL, 'files/catalog/upload/e2230720ffa293a762b33e4364225fa3.gif', 1, NULL, NULL, NULL, NULL),
(17, NULL, NULL, 'files/catalog/upload/09a7fe604c79211411b9c28b0c9cd29b.png', 1, NULL, NULL, NULL, NULL),
(18, NULL, NULL, 'files/catalog/upload/3146e65c453f2f35e3c36035dd1e128c.png', 1, NULL, NULL, NULL, NULL),
(19, NULL, NULL, 'files/catalog/upload/b3d9f2b22f87f7ed1c0c06f6f40b6aec.jpg', 1, NULL, NULL, NULL, NULL),
(20, NULL, NULL, 'files/catalog/upload/3a74c725b5047621fb3d3ee0c27c61e9.png', 1, NULL, NULL, NULL, NULL),
(22, 'Предварительная работа по заявке', '<p>Мы получили заявку от вас. В течение 24 часов мы свяжемся с вами. Наша цель &ndash; сориентировать вас, сможем ли вам помочь и вернуть деньги по вашему ДДУ. В случае необходимости &ndash; мы сделаем выезд на квартиру и проведем предварительный осмотр. Бесплатно для вас.&nbsp;</p>\n', NULL, 1, '24&nbsp;часа', NULL, NULL, NULL),
(23, 'Заключение договора. Осмотр квартиры.', '<p>Мы готовы предложить вам гибкую систему оплаты по нашему договору, вплоть до оплаты всех работ по результату. После заключения договора и первой оплаты, мы планируем выезд специалиста в удобное для вас время. Осмотр квартиры длится от 30 минут. Обычно техническое обследование происходит в присутствие застройщика.&nbsp;</p>\n', NULL, 1, '1&nbsp;неделя', NULL, NULL, NULL),
(24, 'Строительно-техническая экспертиза ', '<p>По результатам осмотра вашей квартиры, мы готовим техническое заключение. В результате спустя неделю после заключения договора вы получаете итоговую стоимость выявленных недостатков. На эту сумму мы предъявляем застройщику претензию&nbsp;</p>\n', NULL, 1, '1&nbsp;неделя', NULL, NULL, NULL),
(25, 'Досудебная работа ', '<p>До того, как обратиться в суд, мы предлагаем застройщику в добровольном порядке уменьшить цену договора долевого участия. Мы вступаем с застройщиком в диалог, обсуждаем выявленные недостатки. Зачастую решение о выплате принимается на этом этапе.</p>\n', NULL, 1, '2&nbsp;недели', NULL, NULL, NULL),
(26, 'Судебный этап', '<p>В случае, если застройщик отказывается осуществить выплату, мы обращаемся в суд. Итоговая сумма по результатам судебного разбирательства может быть изменена.</p>\n', NULL, 1, '4&nbsp;месяца', NULL, NULL, NULL),
(30, '<p>Покупая новую квартиру, вы платите за качественное жилье. Есть ли у вас претензии к качеству своей квартиры в новостройке? Мы поможем выявить строительные, отделочные недостатки и вернуть вам деньги за уже сданное жилье. С нашей стороны – гибкая система оплаты и гарантия результата.</p>\n			<span>Мы решаем широкий круг задач по защите прав дольщиков. </span>', '<p><strong>Всегда к вашим услугам:</strong></p>\n<br />\n<span class="span-all">Бесплатная консультация юриста.<br />\nСвязаться с исполнительным директором - Вы можете<br />\nчерез <a href="tel:+73512232260">звонок с сайта</a>. <a href="#anketa" class="yak">Воспользуйтесь формой нашей заявки:</a> укажите, какие недостатки вы обнаружили в своей квартире, и мы свяжемся с вами, чтобы обсудить это.</span>\n', 'files/catalog/upload/7b14fb906298e1a562c738721b86c0d7.jpg', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `security_role`
--

CREATE TABLE `security_role` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_role`
--

INSERT INTO `security_role` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `security_user`
--

CREATE TABLE `security_user` (
  `id` int(11) NOT NULL,
  `disabled` smallint(6) DEFAULT '0',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_user`
--

INSERT INTO `security_user` (`id`, `disabled`, `name`, `password`) VALUES
(1, 0, 'admin', 'b98a1a5133f9185176cd79d2dda2c428');

-- --------------------------------------------------------

--
-- Структура таблицы `security_user_role`
--

CREATE TABLE `security_user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_user_role`
--

INSERT INTO `security_user_role` (`user_id`, `role_id`) VALUES
(1, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `catalog_section`
--
ALTER TABLE `catalog_section`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `feed`
--
ALTER TABLE `feed`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `feed_item`
--
ALTER TABLE `feed_item`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `forms_field_action`
--
ALTER TABLE `forms_field_action`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `forms_field_type`
--
ALTER TABLE `forms_field_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `infoblock`
--
ALTER TABLE `infoblock`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus_item`
--
ALTER TABLE `menus_item`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_forms`
--
ALTER TABLE `position_forms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_news`
--
ALTER TABLE `position_news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_setting`
--
ALTER TABLE `position_setting`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `redirection`
--
ALTER TABLE `redirection`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_forms`
--
ALTER TABLE `section_forms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_forms_settings`
--
ALTER TABLE `section_forms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_form_feedback`
--
ALTER TABLE `section_form_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_news`
--
ALTER TABLE `section_news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_page`
--
ALTER TABLE `section_page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_role`
--
ALTER TABLE `security_role`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_user`
--
ALTER TABLE `security_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`name`);

--
-- Индексы таблицы `security_user_role`
--
ALTER TABLE `security_user_role`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `catalog_section`
--
ALTER TABLE `catalog_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблицы `feed`
--
ALTER TABLE `feed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `feed_item`
--
ALTER TABLE `feed_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `infoblock`
--
ALTER TABLE `infoblock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `menus_item`
--
ALTER TABLE `menus_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `position_forms`
--
ALTER TABLE `position_forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `position_news`
--
ALTER TABLE `position_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `position_setting`
--
ALTER TABLE `position_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `redirection`
--
ALTER TABLE `redirection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `security_user`
--
ALTER TABLE `security_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
