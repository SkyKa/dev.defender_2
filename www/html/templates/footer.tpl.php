



	<footer>
		<div class="container-fluid">
		<div class="row">
		<div class="container questions" id="questions">
			<h2>Остались вопросы?</h2>
			<div class="col-xs-12 col-sm-4 adress">
				<div>
				<h3>Адрес</h3>
				<p>ЧЕЛЯБИНСК,<br>ЭНТУЗИАСТОВ 28А,<br>ОФИС 601</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 e-mail">
				<div>
				<h3>Email</h3>
				<p><a href="#">oblcentr74@gmail.com</a><br><a href="#">2232260@mail.ru</a></p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 phone">
				<div>
				<h3>Телефон</h3>
				<p><a href="#">+7 (351) 223-22-60</a></p>
				</div>
			</div>
		</div>
		</div>
		</div>
		<div class="container-fluid">
		<div class="row">
		<div class="container">
			<div class="col-xs-12 col-md-6 map">
				<div class="gis_map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2279.8530624460536!2d61.37381131552825!3d55.15084998039664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43c592bcbf7ff753%3A0x98a208fa92565a89!2z0YPQuy4g0K3QvdGC0YPQt9C40LDRgdGC0L7QsiwgMjjQkCwg0KfQtdC70Y_QsdC40L3RgdC6LCDQp9C10LvRj9Cx0LjQvdGB0LrQsNGPINC-0LHQuy4sIDQ1NDA4MA!5e0!3m2!1sru!2sru!4v1461238254207" width="585" height="335" frameborder="0" style="border:0"></iframe>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 feedback">
				<h2>Связаться с нами</h2>
				<form id="ostavit_zayavku_form" method="post">
				<div class="form-group">
					<input type="text" required=""  name="name" placeholder="Имя*">
					<input type="text" required=""  name="phone" placeholder="Телефон*">
					<input type="text" required=""  name="mail" placeholder="E-mail">
					<input  placeholder="Текст сообщения" name="message">
					<button type="submit">Отправить</button>
				</div>
				</form>
			</div>
		</div>
		</div>
		</div>
		<div class="container-fluid">
		<div class="row footer">
		<div class="container">
			<div class="logo-footer col-xs-12 col-sm-2">
				<a href="#"><img src="/static/img/logo_small.png"></a>
			</div>
			<div class="adress-footer col-xs-12 col-sm-4">
				<p>ЧЕЛЯБИНСК, ЭНТУЗИАСТОВ 28А, ОФИС 601<br>
				<a href="#">+7 (351) 223-22-60</a><br>
				<a href="#">oblcentr74@gmail.com</a>,<br>
				<a href="#">2232260@mail.ru</a></p>
			</div>
			<div class="it-footer col-xs-12 col-sm-6">
				<a href="#" target="_blank">
				<p>Cоздание сайта— IT Factory</p>
				<img src="/static/img/logo-factory.png" alt="">
				</a>
			</div>
		</div>
		</div>
		</div>
	</footer>


	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">ЗАДАТЬ ВОПРОС СПЕЦИАЛИСТУ</h4>
		  </div>
		  <div class="modal-body">
			<form id="zadat_vopros_form" method="post">
				<div class="form-group">
					<input type="text" required=""  name="name" placeholder="Имя*">
					<input type="text" required=""  name="phone" placeholder="Телефон*">
					<input type="text" required=""  name="mail" placeholder="E-mail">
					<input  placeholder="Текст сообщения" name="message">
					<button type="submit">Отправить</button>
				</div>
				</form>
		  </div>
		</div>
	  </div>
	</div>


	<script src="https://code.jquery.com/jquery-3.2.1.js"
	  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
	  crossorigin="anonymous">  	
 	</script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript" src="/static/js/jquery.1.7.1.min.js"></script>
	<script src="/static/js/map.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYg29Z7KaocCNIlQWIyf4giZAVw22e3SQ"></script>
	<script src="/static/js/plus.js"></script>
	<script src="/static/js/yakor.js"></script>
	<link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/forms_js.css' )?>">
	<script src="/ajax/?mod=catalog.action.forms_js&debug"></script>
</body>
</html>
