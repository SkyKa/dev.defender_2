<!DOCTYPE html>
<html>
<html lang="en">
  <head>
    <title><?php $title = $registry->title; if($title) echo $title; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="<?php out('description')?>" />
    <meta name="keywords" content="<?php out('keywords')?>" />
    <base href="<?php echo $base ?>" />
    <?php mod('catalog.editor.header')?>
	<link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/forms_js.css' )?>">
    <link rel="shortcut icon" href="/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Описание">
    <title>Центр по защите прав</title>
	<link href="/static/css/style.css" rel="stylesheet">
	<link href="/static/css/media.css" rel="stylesheet">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  </head>
<body>
	<header>
		<div class="container-fluid">
		<div class="row header-line">
		<div class="container">
			<div class="header-logo col-xs-12 col-sm-3">
				<a href="#"><img src="/static/img/logo_small.png"></a>
			</div>
			<div class="header-title col-xs-12 col-sm-9">
				<a href="#"><h1>Областной центр по защите прав</h1></a>
			</div>
		</div>
		</div>
		</div>
	</header>
	<div class="container-fluid whyme" id="header">
	<div class="row">
	<div class="container">
		<ul class="nav-ul">
			<li><a href="#wrap2" class="yak">ПОЧЕМУ МЫ</a></li>
			<li><a href="#jdun" class="yak">НАМ ДОВЕРЯЮТ</a></li>
			<li><a href="#coop" class="yak">ЭТАПЫ РАБОТ</a></li>
			<li><a href="#anketa" class="yak">ЗАПОЛНИТЕ ЗАЯВКУ</a></li>
			<li><a href="#questions" class="yak">КОНТАКТЫ</a></li>
		</ul>
	</div>
	</div>
	</div>