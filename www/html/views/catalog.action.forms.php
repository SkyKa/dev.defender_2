<?php

	$str = '';

	$form_alias = $args -> alias;

	$form_values = $args -> values;

	$form = Form :: getInstance( );
	$form -> setRows( $form_alias );
	if ( $form -> errorInfo ) {
		echo $form -> errorInfo;
		exit;
	}
	$section = $form -> getSection( );

	$table = new Table( 'position_forms' );

	$rows = $table -> select( "SELECT * FROM `position_forms` WHERE `section_id`=:sid ORDER BY `position`", array( 'sid' => $section[ 'id' ] ) );

	if ( $section[ 'html_id' ] ) $html_id = " id='" . $section[ 'html_id' ] . "' ";
	else $html_id = '';

	//$str .= val( 'catalog.editor.btn_section_onlyedit', array( 'section_id' => $section[ 'id' ] ) );
	//$str .= val( 'catalog.editor.btn_position_add', array( 'section_id' => $section[ 'id' ] ) );

	$faction = ( !$section[ 'action' ] ) ? '' : $section[ 'action' ];

	$method = str_replace( 'ajax-', '', $section[ 'method' ] );

	$str .= "
	<form" . $html_id . " method='" . $method . "' novalidate ";
	$multipart = "";
	foreach( $rows as $row ) {
		$multipart = "";
		if ( $row[ 'type_id' ] == 'upload' || $row[ 'type_id' ] == 'file' ) {
			$multipart = " enctype='multipart/form-data'";
			break;
		}
	}
	$str .= $multipart;
	$str .= ">";



	$str .= "
	<input type='hidden' name='form_act' value='" . $section[ 'action' ] . "' />
	<input type='hidden' name='form_alias' value='" . $form_alias . "' />";
	
		foreach ( $rows as $row ) {
			$str .= $form -> display( $row, $section, $form_values );
		}

	$str .= "
	</form>";

	//}

	echo $str;
