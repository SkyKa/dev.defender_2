<?php

	$pay_order_id = ( isset( $_GET[ 'orderId' ] ) ) ? $_GET[ 'orderId' ] : 'none';

	$pay = Registry :: __instance() -> pay;
	$order = $pay -> get_order( array( 'pay_order_id' => $pay_order_id ) );

	if ( !count( $order ) ) return false;

	$pay_status = $pay -> status( $pay_order_id );
	if ( isset( $pay_status[ 'OrderStatus' ] ) ) {
		echo $pay -> order_status[ $pay_status[ 'OrderStatus' ] ];
	}
	
	echo '<p>&nbsp;</p>';