<?php

	$alias = 'chasto-zadavaemye-voprosy';

	$table = new Table( 'catalog_section' );
	
	$section = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $alias ) );
	if ( !count( $section ) ) return false;
	$section = end( $section );

	$section_parents = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:pid ORDER BY `position`', array( 'pid' => $section[ 'id' ] ) );
	if ( !count( $section_parents ) ) return false;
	
	
	$arr = array( 'One', 'Two', 'Tree', 'Four' );
	$arr2 = array( 'fist', 'sec', 'thir', 'forur' );
	
	
	foreach ( $section_parents as $key => $section_parent ) {

		$section_page = $table -> select( 'SELECT * FROM `' . $section_parent[ 'section_table' ] . '` WHERE `id`=:id LIMIT 1', array( 'id' => $section_parent[ 'id' ] ) );
		if ( !count( $section_page ) ) return false;
		$section_page = end( $section_page );

		//var_dump( $section_page );
		
		if ( !$key ) {
			$id = 'accordion';
		} else {
			$id = 'accordion' . $key + 1;
		}
		
		echo '
		<div class="panel-group" id="' . $id . '" role="tablist" aria-multiselectable="true">
			  <div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="heading' . $arr[ $key ] . '">
			      <h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#' . $id . '" href="#collapse' . $arr[ $key ] . '" aria-expanded="true" aria-controls="collapse' . $arr[ $key ] . '" id="' . $arr2[ $key ] . '">
			          <span class="znak' . ( $key + 1 ) . '"> ' . ( ( !$key ) ? '-' : '+' ) . ' </span>' . $section_page[ 'small_text' ] . '
			        </a>
			      </h4>
			    </div>
			    <div id="collapse' . $arr[ $key ] . '" class="panel-collapse collapse' . ( ( !$key ) ? ' in' : '' ) . '" role="tabpanel" aria-labelledby="heading' . $arr[ $key ] . '">
			      <div class="panel-body">
					' . $section_page[ 'content' ] . '
			      </div>
			    </div>
			  </div>			 
		</div>';
		

	}
	