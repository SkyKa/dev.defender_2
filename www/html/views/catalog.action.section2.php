<?php

	$alias = 'pochemu-my';

	$table = new Table( 'catalog_section' );
	
	$section = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $alias ) );
	if ( !count( $section ) ) return false;
	$section = end( $section );

	$section_parents = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:pid ORDER BY `position`', array( 'pid' => $section[ 'id' ] ) );
	if ( !count( $section_parents ) ) return false;
	
	
	$arr = array(
		0 => array(
			'cercle' => false,
			'wrap' => 'wrap-team col-xs-12 col-sm-offset-4 col-sm-4',
			'title' => 'team'
		),
		1 => array(
			'cercle' => false,
			'wrap' => 'wrap-otv col-xs-12 col-sm-4',
			'title' => 'otvet'
		),
		2 => array(
			'cercle' => false,
			'wrap' => 'wrap-komp col-xs-12 col-sm-offset-4 col-sm-4',
			'title' => 'comp'
		),
		3 => array(
			'cercle' => false,
			'wrap' => 'wrap-exp col-xs-12 col-sm-offset-4 col-sm-4',
			'title' => 'exp'
		)

	);
	

?>

				<h2><?php echo $section[ 'title' ]?></h2>
				<div class="krug"></div>
				<div class="krug2"></div>
				<div class="krug3"></div>
				
				
<?php


	foreach ( $section_parents as $key => $section_parent ) {

		$section_page = $table -> select( 'SELECT * FROM `' . $section_parent[ 'section_table' ] . '` WHERE `id`=:id LIMIT 1', array( 'id' => $section_parent[ 'id' ] ) );
		if ( !count( $section_page ) ) return false;
		$section_page = end( $section_page );

		
		echo '
			<div class="' . $arr[ $key ][ 'wrap' ] . '">
				<h3>' . $section_parent[ 'title' ] . '</h3>';

		if ( $arr[ $key ][ 'cercle' ] ) {

			echo '
				<div class="' . $arr[ $key ][ 'cercle' ] . '">';

		}

		echo '
					<img src="/' . $section_page[ 'img' ] . '" class="' . $arr[ $key ][ 'title' ] . '">';

		if ( $arr[ $key ][ 'cercle' ] ) {

			echo '
				</div>';

		}

		echo '
				' . $section_page[ 'small_text' ] . '
			</div>';

		if ( !$key ) {

			echo '<div class="col-sm-4 fix"></div>';

		}


	}

?>