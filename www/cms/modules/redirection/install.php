<?php
/**
 *
 */

class RedirectionInstall extends Install_Base
{
    public $title = "Перенаправление";

    public $sql = '
CREATE TABLE IF NOT EXISTS `redirection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(250) COLLATE utf8_unicode_ci,
  `count` int(11),
  `img_src` varchar(250) COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;
';

   // public $dirs = array('banner');
}

?>
