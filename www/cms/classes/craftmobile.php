<?php
/**
 * Description of SendSMS class
 *
 * @author Startcev-PV
 *
 *   
 * -------------------------
 * Пример использования:
 *  
 *    $sms = new Sendsms( );
 *    $sms->setLogin( 'логин' );
 *    $sms->setPassword( 'пароль' );
 *    $sms->setSender( 'подпись' );
 *    $sms->setPhone( 'Номер телефона' );
 *    $sms->setText( 'Привет Мир! =)' );
 *    $sms->send( );
 *     
 *    echo $sms->smsinfo;
 *    
 */


  class Craftmobile  {


    /** -- статусы сообщений smsinfo */
    
    /** Сообщение было запланировано. Доставка до сих пор не начата */
    const SCHEDULED         = 100;
    /**  Сообщение находится в состоянии по маршруту */
    const ENROUTE           = 101;
    /** Сообщение будет доставлено к месту назначения */
    const DELIVERED         = 102;
    /** Сообщение в силе период истек */
    const EXPIRED           = 103;
    /** Сообщение удалено */
    const DELETED           = 104;
    /** Сообщение не доставлено */
    const UNDELIVERABLE     = 105;
    /** Сообщение в принятом состоянии (т.е. был вручную прочитал от имени абонента обслуживания клиентов) */
    const ACCEPTED          = 106;
    /** Сообщение находится в недопустимом состоянии, состояние сообщения - неизвестно. */
    const UNKNOWN           = 107;
    /** Сообщение находится в состоянии - сообщение отклонено, было отвергнуто интерфейсом доставки */
    const REJECTED          = 108; 
    /** сообщение отбрасывается */
    const DISCARDED         = 109;


    /** -- статусы ошибок */
    
    /** Неизвестная ошибка */
    const ERR_UNKNOWN       = 200;
    /** Неправильный ID сообщения */ 
    const ERR_ID            = 201;
    /** Неправильный идентификатор отправителя */
    const ERR_SENDER        = 202;
    /** Неправильный номер получателя */
    const ERR_RECIPIENT     = 203;
    /** Слишком длинное или пустое сообщение */
    const ERR_LENGTH        = 204;
    /** Пользователь отключен */
    const ERR_USER_DISABLE  = 205;
    /** Ошибка биллинга */
    const ERR_BILLING       = 206;
    /** Превышение лимита выделенных сообщений */
    const ERR_OVERLIMIT     = 207;



    /** сюда скидывается статус, после отправки сообщения */
    public $smsinfo = NULL;



    /** путь до API */
    protected $_url = "http://service.craftmobile.ru/get.ashx";
    /** файл API */
    protected $_get = "";

    /** параметр принимает значение логина пользователя в системе */
    private $_login = NULL;
    /** параметр принимает значение пароля, соответствующего логину */
    private $_password = NULL;
    /** для отправки сообщения данный параметр должен принимать значение  - ‘message’ */
    private $_type = 'message';
    /** (integer) пользовательский числовой идентификатор сообщения,
     *  необязательный атрибут,
     *  при использовании пользователь должен гарантировать уникальность
     *  данного идентификатора в пределах своей учетной записи */
    private $_id = NULL;
    /** (varchar(21)) получатель сообщения (номер телефона),
     *  может использоваться в формате с «+» или без него. */
    private $_phone = NULL;
    /** (varchar(11)) отправитель сообщения (подпись сообщения),
     *  необязательный атрибут */
    private $_sender = NULL;
    /** параметр содержит текст сообщения */ 
    private $_text = NULL; 



    public function __construct( ) {
      ;
    }




    /** получить логин */
    public function getLogin( ) {
      return $this->_login;
    }

    /** изменить логин */
    public function setLogin( $login=NULL ) {
      if ( !is_null( $login ) ) $this->_login = $login;
    }


    /** получить пароль */
    public function getPassword( ) {
      return $this->_password;
    }

    /** изменить пароль */
    public function setPassword( $password=NULL ) {
      if ( !is_null( $password ) ) $this->_password = $password;
    }


    /** получить отправителя */
    public function getSender( ) {
      return $this->_sender;
    }

    /** изменить отправителя */
    public function setSender( $sender=NULL ) {
      if ( !is_null( $sender ) ) $this->_sender = $sender;
    }


    /** изменить тип действия */
    public function setType( $type=NULL ) {
      if ( !is_null( $type ) ) $this->_type = $type;
    }



    /** изменить числовой идентификатор сообщения */
    public function getId( ) {
      return $this->_id;
    }

    /** изменить числовой идентификатор сообщения */
    public function setId( $id=NULL ) {
      if ( !is_null( $id ) ) $this->_id = $id;
    }



    /** получить номер телефона */
    public function getPhone( ) {
      return $this->_phone;
    }

    /** изменить номер телефона */
    public function setPhone( $phone=NULL ) {
      if ( !is_null( $phone ) ) $this->_phone = $phone;
    }



    /** получить текст сообщения */
    public function getText( ) {
      return $this->_phone = $text;
    }

    /** изменить текст сообщения */
    public function setText( $text=NULL ) {
      if ( !is_null( $text ) ) $this->_text = $text;
    }



    /** метод отправки SMS сообщения */
    public function send( ) {

        /** блок исключений, проверка сохранности свойств класса */
        if ( is_null( $this->_login ) )
          throw new Exception( "param '_login' is NULL, use ->setLogin( 'login' ) " );
        if ( is_null( $this->_password ) )
          throw new Exception( "param '_password' is NULL, use ->setPassword( 'password' ) " );
        if ( is_null( $this->_phone ) )
          throw new Exception( "param '_phone' is NULL, use ->setPhone( '89...' ) " );
        if ( is_null( $this->_sender ) )
          throw new Exception( "param '_sender' is NULL, use ->setSender( 'sender' ) " );
        if ( is_null( $this->_text ) )
          throw new Exception( "param '_text' is NULL, use ->setText( 'smsText...' ) " );
        if ( is_null( $this->_type ) )
          throw new Exception( "param '_type' is NULL, use ->setType( 'message' ) " );


        /** запрос на сервер API */
        $resp = $this->_url;
        $resp_get = $this->_get;
        $resp_get .= "?login=" . $this->_login;
        $resp_get .= "&password=" . $this->_password;
        $resp_get .= "&recipient=" . $this->_phone;
        $resp_get .= "&sender=" . $this->_sender;
        $resp_get .= "&text=" . urlencode( $this->_text );
        $resp_get .= "&type=" . $this->_type;

        if ( !is_null( $this->_id ) )
          $resp_get .= "&id=" . $this->_id;

        /** запрос */
        $contents = file_get_contents( $this->_url . $resp_get, 'r' );

        $ex_contents = explode( '=', $contents );
        /** записываем состояние в smsinfo */
        $this->smsinfo = $ex_contents[ 1 ];

        return $contents;

    }



  }