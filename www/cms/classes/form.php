<?php

class Form {

	public $errorInfo;
	protected static $_instance;

	protected $_alias = 'forms_section';

	protected $_section = array( );
	protected $_rows = array( );
	protected $_settings = array( );

	public static function getInstance( ) {
		if ( null === self :: $_instance ) {
			self :: $_instance = new self( );
		}
		return self :: $_instance;
	}


	public function __construct( ) {

		$table = new Table( 'section_forms' );

		$settings = $table -> select(

				"SELECT  `t2`.*, `t1`.`parent_id`,`t1`.`title`,`t1`.`position`,`t1`.`alias`,`t1`.`position_table`,`t1`.`section_table`,`t1`.`children_tpl`,`t1`.`leaf`
				FROM `catalog_section` `t1` 
				LEFT JOIN `section_forms_settings` `t2` 
				ON `t1`.`id`=`t2`.`id` 
				WHERE `t1`.`alias`=:alias LIMIT 1",

				array( 'alias' => $this -> _alias )
		);

		if ( count( $settings ) ) {
			$settings = end( $settings );
			$this -> _settings = $settings;
		}

	}

	public function __destruct( ) {
		;
	}


	// Установить поля формы по алиасу
	public function setRows( $alias=null ) {

		$table = new Table( 'section_forms' );
		$catalog_section = $table -> select( "SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1",
								array( 'alias' => $alias ) );

		if ( !count( $catalog_section ) ) {
			$this -> errorInfo = 'alias does not exist';
			return false;
		} else $sid = $catalog_section[ 0 ][ 'id' ];

		$table = new Table( 'section_forms' );

		$sections = $table -> select( "SELECT * FROM `section_forms` WHERE `id`=:id LIMIT 1",
								array( 'id' => $sid ) );
		if ( !count( $sections ) ) {
			$this -> errorInfo = 'section_form does not exist';
			return false;
		}
		$this -> _section = $sections[ 0 ];

		$rows = $table -> select( "SELECT * FROM `position_forms` WHERE `section_id`=:sid ORDER BY `position`",
							array( 'sid' => $sid ) );
		if ( !count( $sections ) ) {
			$this -> errorInfo = 'position_forms does not exist';
			return false;
		}
		$this -> _rows = $rows;
	}


	public function getRows( ) {
		return $this -> _rows;
	}

	public function getSection( ) {
		return $this -> _section;
	}

	public function getSettings( ) {
		return $this -> _settings;
	}


	// получить аргументы формы
	public function getArgs( ) {
		$rows = $this -> getRows( );
		if ( !count( $rows ) ) throw new Exception( 'this method not access, setRows cant be used' );
		$args = array( );
		foreach ( $rows as $row ) {
			if (	$row[ 'type_id' ] == 'link' ||
					$row[ 'type_id' ] == 'submit' ) continue;

			$key = ( string ) $row[ 'nameid' ];
			$args[ $key ] = Utils :: getVar( $key );
		}
		return $args;
	}

	// валидация формы
	public function valide( ) {
		$rows = $this -> getRows( );
		if ( !count( $rows ) ) throw new Exception( 'this method not access, setRows cant be used' );
		$args = $this -> getArgs( );
		$errors = array( );
		$pwd = array( );
		foreach ( $rows as $row ) {
			if ( $row[ 'type_id' ] == 'pwd' ) {
				if ( mb_strlen( $args[ $row[ 'nameid' ] ] ) < 5 ) $errors[ $row[ 'nameid' ] ][ ] = "Пароль не должен быть менее 5ти символов";
				$pwd[ ] = $row[ 'nameid' ]; // скидываем поля с паролями в массив
			}
			if ( !$row[ 'valid_empty' ] ) continue; // если поле не обязательное, мимо
			if ( !$args[ $row[ 'nameid' ] ] ) { // если значение поля не было передано с формы, обижаемся
				$errors[ $row[ 'nameid' ] ][ ] = "Введите &laquo;" . $row[ 'name' ] . "&raquo;"; // копим злость в массиве ошибок!
				// если нужна валидация на e-mail
				if ( $row[ 'valid_email' ] ) {
					// ищем подвох
					if ( !preg_match( "/[0-9a-zA-Z_\-]+@[0-9a-zA-Z_\.\-]+\.[a-zA-Z_]{2,3}/i", $row[ 'email' ] ) )
						$errors[ $row[ 'nameid' ] ][ ] .= "Адрес электронной почты введен неверно";
				}
			}
		}
		// 2 поля с паролем - регистрация
		$cnt = count( $pwd );
		if ( $cnt == 2 ) {
			if ( $pwd[ 0 ] != $pwd[ 1 ] ) {
				$errors[ 'system' ][ ] .= "Пароль и подтверждение пароля не совпадают";
			}
		}
		// 3 поля с паролем - сменить пароль в ЛК
		else if ( $cnt == 3 ) {
			if ( $pwd[ 1 ] != $pwd[ 2 ] ) {
				$errors[ 'system' ][ ] .= "Пароль и подтверждение пароля не совпадают";
			}
		}
		return $errors;
	}


	// отрисовка формы
	public function display( $row, $section, $values=array( ) ) {

		$str = '';

		$key = mb_strtolower( $row[ 'nameid' ] );

		if ( !isset( $values[ $key ] ) ) {
			$val = ( !isset( Registry :: __instance( ) -> FormArgs[ $key ] ) ) ? NULL : Registry :: __instance( ) -> FormArgs[ $key ];
		} else $val = $values[ $key ];

		$row[ 'name' ]  = ( $row[ 'valid_empty' ] ) ? $row[ 'name' ] . '*' : $row[ 'name' ];

		$str .= $row[ 'html_before' ];

		switch ( $row[ 'type_id' ] ) {


			case 'label':

				$str .= "
					<p class='form-control'>" . $row[ 'name' ] . "</p>";

			break;


			case 'radiobox':

				$id_radiobox = 'radiobox_' . Utils :: translit( $row[ 'name' ] );

				$str .= "
					<input type='radio' checked class='form-control radio' name='" . $row[ 'nameid' ] . "' value='" . substr( $row[ 'name' ], 0, -1 ) . "' id='" . $id_radiobox . "' />
					<label for='" . $id_radiobox . "' class='radio-" . $id_radiobox . "'>" . $row[ 'name' ] . "</label>";

			break;


			case 'text':

				// val( 'catalog.editor.btn_position', array( 'section_id' => $section[ 'id' ], 'position_id' => $row[ 'id' ] ) );
				if ( $row[ 'select_options' ] ) {
				}
				$str .= "
						<input type='text' required='' class='form-control' name='" . $row[ 'nameid' ] . "' placeholder='" . $row[ 'name' ] . "' />";

			break;


			case 'memo':

				if ( $row[ 'select_options' ] ) {
				}

				$str .= "
					<textarea placeholder='" . $row[ 'name' ] . "' name='" . $row[ 'nameid' ] . "' class='form-control' required=''></textarea>";

			break;


			case 'select':

				$options = explode( "\n", $row[ 'select_options' ] );
				$str .= "
						<select class='form-control' name='" . $row[ 'nameid' ] . "'>
							<option value='' disabled selected hidden>" . $row[ 'name' ] . "</option>";
				if ( count( $options ) ) {
					foreach ( $options as $option ) {
						$str .= "
							<option>" . $option . "</option>";
					}
				}
				$str .= "
						</select>";
			break;


			case 'upload':

				$str .= "
						<input type='file' name='" . $row[ 'nameid' ] . "' type='file' name='" . $row[ 'nameid' ] . "[]' id='" . $row[ 'nameid' ] . "' multiple='true'  accept='" . $row[ 'select_options' ] . "' />";
			break;


			case 'date':

				if ( $row[ 'select_options' ] ) {
					$str .= "
						<div class='b_afb_box'>" . $row[ 'select_options' ] . "</div>";
				}

			break;


			case 'pwd':

				if ( $row[ 'select_options' ] ) {
				}

				$str .= "
					<input type='password' class='form-control' placeholder='" . $row[ 'name' ] . "' name='" . $row[ 'nameid' ] . "' />";

			break;


			case 'check':

				if ( $val ) $check = ' CHECKED';
				else $check = '';

				$str .= "
						<input required type='checkbox' class='checkbox form-control hidden-field' name='" . $row[ 'nameid' ] . "' id='" . $row[ 'nameid' ] . "' />
						<label for='" . $row[ 'nameid' ] . "' class='check-mark'></label>
						<p>" . $row[ 'name' ] . "</p>";

				break;

			case 'link':
				$str .= "
				<a data-toggle='modal' data-target='#" . $row[ 'nameid' ] . "' class='" . $row[ 'nameid' ] . "' href='#' data-dismiss='modal' aria-hidden='true'>" . $row[ 'name' ] . "</a>";

			break;


			case 'hidden':
				$str .= "
							<input type='hidden' name='" . $row[ 'nameid' ] . "' value='" . $row[ 'name' ] . "' />";
			break;


			case 'submit':

				$str .= "
					<button type='submit'>" . $row[ 'name' ] . "</button>";

				break;

		}

		$str .= $row[ 'html_after' ];

		return $str;

	}



	public static function parse_mail_tpl( $tpl='', $args=array() ) {

		if ( !count( $args ) ) return $tpl;

		$pattern = "/{[^}]*}/";
		preg_match_all( $pattern, $tpl, $matches );

		$matches = end( $matches );
		$arr_key = array( );

		foreach ( $matches as $key ) {

			$key = str_replace( '{', '', $key );
			$key = str_replace( '}', '', $key );
			$key_lw = mb_strtolower( $key );

			if ( !isset( $args[ $key_lw ] ) ) $replace = '';
			else $replace = $args[ $key_lw ];

			$tpl = str_replace( '{' . $key . '}', $replace, $tpl );

		}

		return $tpl;

	}



}
