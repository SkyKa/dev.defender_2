<?php

/*
SELECT `t1`.`id` `id`, `t1`.`anonce` `t_anonce`, `t1`.`banner` `t_banner`, `t1`.`ico` `t_ico`,
			`t1`.`ico_small` `t_ico_small`, `t1`.`description` `t_description`,
			`t2`.`parent_id`, `t2`.`title`, `t2`.`position`,	`t2`.`alias`, `t2`.`position_table`, `t2`.`section_table`, `t2`.`children_tpl`,`t2`.`leaf`
			FROM `section_theme` `t1` LEFT JOIN `catalog_section` `t2`
			ON `t1`.`id`=`t2`.`id` ORDER BY `t2`.`position`


SELECT `t1`.`id`,`t1`.`title`,`t1`.`progress`,`t1`.`section_id`,
			`t2`.`position_table`,`t2`.`id` `s_id`, `t2`.`parent_id`, `t3`.`title` as `theme`
			FROM `position_history` `t1` LEFT JOIN `catalog_section` `t2`
			ON `t1`.`section_id`=`t2`.`id` LEFT JOIN `catalog_section` `t3`
			ON `t2`.`parent_id`=`t3`.`id` WHERE `t2`.`position_table`="position_history" ORDER BY `t3`.`id`;
*/


	class Lesson_theme {

		protected static $db;

		protected static $_instance;

		public $errorInfo;


		public function __construct( ) {
			self :: $db = new Table( 'section_theme' );
		}


		public static function getInstance( ) {
			if ( null === self :: $_instance ) {
				self :: $_instance = new self( );
			}
			return self :: $_instance;
		}


		/** ������� ������ ��� */
		public function getThemeList( ) {

			$sql = 'SELECT `t1`.`id` `id`, `t1`.`anonce` `t_anonce`, `t1`.`banner` `t_banner`, `t1`.`ico` `t_ico`,
			`t1`.`ico_small` `t_ico_small`, `t1`.`description` `t_description`,
			`t2`.`parent_id`, `t2`.`title`, `t2`.`position`,	`t2`.`alias`, `t2`.`position_table`, `t2`.`section_table`, `t2`.`children_tpl`,`t2`.`leaf`
			FROM `section_theme` `t1` LEFT JOIN `catalog_section` `t2`
			ON `t1`.`id`=`t2`.`id` ORDER BY `t2`.`position`';

			$rows = self :: $db -> select( $sql );

			if ( count( $rows ) ) return $rows;

			$this -> errorInfo = "not fined theme list";

			return array( );
		}

		/** ������� section_id ��� */
		public function getThemesSid( ) {
			$rows = self :: $db -> select( 'SELECT `id` as `s_id`
			FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'training' ) );
			if ( !count( $rows ) ) {
				$this -> errorInfo = "not fined section_id";
				return false;
			}
			return $rows[ 0 ][ 's_id' ];
		}



		
		/** �������� ���������� "��� ���� ��� ���� ����" */
		public function getPage( $id ) {

			$rows = self :: $db -> select( 'SELECT `t2`.`title`,`t1`.`content`,`t1`.`visible`,`t1`.`id`,
			`t2`.`section_table`,`t2`.`id` `s_id`, `t2`.`parent_id`
			FROM `section_page` `t1` LEFT JOIN `catalog_section` `t2`
			ON `t1`.`id`=`t2`.`id` WHERE `t2`.`parent_id`=:id && `t2`.`section_table`="section_page" && `t1`.`visible`=1
			LIMIT 1', array( 'id' => $id ) );
/*
			echo "<pre>";
			var_dump( $id );
			var_dump( self :: $db -> errorInfo );
			var_dump( $rows );
			echo "</pre>";
*/
			if ( !count( $rows ) ) {
				$this -> errorInfo = "not fined theme 'id'=" . $id;
				return array( );
			}
			$list = $rows[ 0 ];

			return $list;
		}
		
		/** �������� section_id ���������� "��� ���� ��� ���� ����" */
		public function getPageSid( $id ) {
			$rows = self :: $db -> select( 'SELECT `id` as `s_id`, `position_table`
			FROM `catalog_section` WHERE `parent_id`=:id &&
			`section_table`="section_page" LIMIT 1', array( 'id' => $id ) );
			if ( !count( $rows ) ) {
				$this -> errorInfo = "not fined theme 'id'=" . $id;
				return false;
			}
			return $rows[ 0 ][ 's_id' ];
		}
		
		

		/** �������� ���������� ��������� �� id ���� */
		public function getInformationList( $id ) {
			$rows = self :: $db -> select( 'SELECT `t1`.`id`,`t2`.`parent_id`,`t2`.`position_table`,
			`t2`.`id` `s_id`, `t2`.`parent_id`
			FROM `section_theme` `t1` LEFT JOIN `catalog_section` `t2`
			ON `t1`.`id`=`t2`.`parent_id` WHERE `t2`.`parent_id`=:id && `t2`.`position_table`="position_information"
			LIMIT 1', array( 'id' => $id ) );

			if ( !count( $rows ) ) {
				$this -> errorInfo = "not fined theme 'id'=" . $id;
				return array( );
			}
			$row = $rows[ 0 ];

			$list = self :: $db -> select( 'SELECT * FROM `position_information` WHERE `section_id`=:sid && `visible`=1 ORDER BY `position` LIMIT 1000', array( 'sid' => $row[ 's_id' ] ) );

			if ( !count( $list ) ) {
				$this -> errorInfo = "not fined information list";
				return array( );
			}
			return $list;
		}


		/** �������� section_id ���������� ��������� */
		public function getInformationSId( $id ) {
			$rows = self :: $db -> select( 'SELECT `id` as `s_id`, `position_table`
			FROM `catalog_section` WHERE `parent_id`=:id &&
			`position_table`="position_information" LIMIT 1', array( 'id' => $id ) );
			if ( !count( $rows ) ) {
				$this -> errorInfo = "not fined theme 'id'=" . $id;
				return false;
			}
			return $rows[ 0 ][ 's_id' ];
		}
		
		

		/** �������� ������ ����������� �� id ���� */
		public function getVideoList( $id ) {
			$rows = self :: $db -> select( 'SELECT `t1`.`id`,`t1`.`section_id`,
			`t2`.`position_table`,`t2`.`id` `s_id`, `t2`.`parent_id`
			FROM `position_video` `t1` LEFT JOIN `catalog_section` `t2`
			ON `t1`.`section_id`=`t2`.`id` WHERE `t2`.`parent_id`=:id &&
			`t2`.`position_table`="position_video" LIMIT 1', array( 'id' => $id ) );

			if ( !count( $rows ) ) {
				$this -> errorInfo = "not fined theme 'id'=" . $id;
				return array( );
			}

			$row = $rows[ 0 ];
			$list = self :: $db -> select( 'SELECT * FROM `position_video` WHERE `section_id`=:sid ORDER BY `position` LIMIT 1000', array( 'sid' => $row[ 'section_id' ] ) );
			if ( !count( $list ) ) {
				$this -> errorInfo = "not fined video-lesson list";
				return array( );
			}
			
			return $list;

		}

		/** �������� section_id ����������� */
		public function getVideoSid( $id ) {
			$rows = self :: $db -> select( 'SELECT `id` as `s_id`, `position_table`
			FROM `catalog_section` WHERE `parent_id`=:id &&
			`position_table`="position_video" LIMIT 1', array( 'id' => $id ) );
			if ( !count( $rows ) ) {
				$this -> errorInfo = "not fined video 'id'=" . $id;
				return false;
			}
			return $rows[ 0 ][ 's_id' ];
		}
		
		
		public function getHistoryById( $id ) {
			$history = self :: $db -> select( 'SELECT * FROM `position_history`
				WHERE `id`=:id LIMIT 1',
				array( 'id' => $id ) );
			if ( count( $history ) ) return end( $history );
			else return false;
		}
		
		

		/** �������� ������ ����������� �������� �� id ���� */
		public function getHistoryList( $id ) {
			$rows = self :: $db -> select( 'SELECT `t1`.`id`,`t1`.`section_id`,
			`t2`.`position_table`,`t2`.`id` `s_id`, `t2`.`parent_id`
			FROM `position_history` `t1` LEFT JOIN `catalog_section` `t2`
			ON `t1`.`section_id`=`t2`.`id` WHERE `t2`.`parent_id`=:id &&
			`t2`.`position_table`="position_history" LIMIT 1', array( 'id' => $id ) );

			if ( !count( $rows ) ) {
				$this -> errorInfo = "not fined theme 'id'=" . $id;
				return array( );
			}

			$row = $rows[ 0 ];

			$list = self :: $db -> select( 'SELECT * FROM `position_history` WHERE `section_id`=:sid ORDER BY `position` LIMIT 1000', array( 'sid' => $row[ 'section_id' ] ) );
/*
			echo "<pre>";
			var_dump( self :: $db -> errorInfo );
			var_dump( $list );
			echo "</pre>";
*/
			if ( !count( $list ) ) {
				$this -> errorInfo = "not fined history list";
				return array( );
			}
			return $list;
		}
		
		
		/** �������� section_id ����������� */
		public function getHistorySid( $id ) {
			$rows = self :: $db -> select( 'SELECT `id` as `s_id`, `position_table`
			FROM `catalog_section` WHERE `parent_id`=:id &&
			`position_table`="position_history" LIMIT 1', array( 'id' => $id ) );
			if ( !count( $rows ) ) {
				$this -> errorInfo = "not fined video 'id'=" . $id;
				return false;
			}
			return $rows[ 0 ][ 's_id' ];
		}


		/** �������� ������ ���������� */
		public function getProgress( $lid ) {

/*
			SELECT `t1`.`id`,`t1`.`section_id`,
			`t2`.`position_table`,`t2`.`id` `s_id`, `t2`.`parent_id`
			FROM `position_video` `t1` LEFT JOIN `catalog_section` `t2`
			ON `t1`.`section_id`=`t2`.`id` WHERE `t2`.`parent_id`=:id &&
			`t2`.`position_table`="position_video" LIMIT 1
*/

			$rows = self :: $db -> select( 'SELECT * FROM `position_learner_lesson` WHERE learner_id=:lid ',
					array( 'lid' => $lid ) );
			return $rows;
		}
	

		// ���������� ����� ���������� ����������
		// theme - id ����		
		public function getHistorysCount( $theme=null ) {
			if ( $theme ) {
				$row_theme = self :: $db -> select( 'SELECT `t1`.`id`,`t1`.`section_id`,
			`t2`.`position_table`,`t2`.`id` `s_id`, `t2`.`parent_id`
			FROM `position_history` `t1` LEFT JOIN `catalog_section` `t2`
			ON `t1`.`section_id`=`t2`.`id` WHERE `t2`.`parent_id`=:id &&
			`t2`.`position_table`="position_history" LIMIT 1', array( 'id' => $theme ) );

				//var_dump( $row_theme );

				if ( count( $row_theme ) ) {
					$rows = self :: $db -> select( 'SELECT COUNT( * ) as `cnt`
					FROM `position_history` WHERE `section_id`=:sid',
					array( 'sid' => $row_theme[ 0 ][ 's_id' ] ) );
				}
				else $rows = array( );
			}
			else {
				$rows = self :: $db -> select( 'SELECT COUNT( * ) as `cnt` FROM `position_history`' );
			}

			if ( count( $rows ) ) {
				return $rows[ 0 ][ 'cnt' ];
			}
			else return 0;
		}
	
	
		// ���������� ���������� �������� ����������
		public function getSessionHistorysCount( $lid=null, $theme=null ) {
			if ( !$lid ) throw new Exception( 'not fined 1 argument learner_id' );
			if ( $theme ) {

				$row_theme = self :: $db -> select( 'SELECT `t1`.`id`,`t1`.`section_id`,
			`t2`.`position_table`,`t2`.`id` `s_id`, `t2`.`parent_id`
			FROM `position_history` `t1` LEFT JOIN `catalog_section` `t2`
			ON `t1`.`section_id`=`t2`.`id` WHERE `t2`.`parent_id`=:id &&
			`t2`.`position_table`="position_history" LIMIT 1', array( 'id' => $theme ) );

				if ( count( $row_theme ) ) {

					$activity = $this -> getSessionActivity( $lid );

					$rows[ 0 ][ 'cnt' ] = 0;

					$history = self :: $db -> select( 'SELECT `id`
					FROM `position_history` WHERE `section_id`=:sid',
					array( 'sid' => $row_theme[ 0 ][ 's_id' ] ) );

					foreach( $history as $h ) {
						if ( isset( $activity[ $h[ 'id' ] ] ) ) ++$rows[ 0 ][ 'cnt' ];
					}
				}
				else $rows = array( );
			}
			else {
				$rows = self :: $db -> select( 'SELECT COUNT( * ) as `cnt` FROM `position_learner_lesson`
					WHERE learner_id=:lid && `credit`=1',
						array( 'lid' => $lid ) );
			}
			
			if ( count( $rows ) ) {
				return $rows[ 0 ][ 'cnt' ];
			}
			else return 0;
		}


		// ������� �������� ����� ��������� �����
		public function saveLookLesson( $theme ) {

			$auth = Auth :: getInstance( );
			$sess = $auth -> isAuth( );
			if ( $sess ) {
				$sections = self :: $db -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1',
						array( 'alias' => 'look_lesson' ) );
				if ( !count( $sections ) ) throw new Exception( 'section look_lesson not found' );
				$section = end( $sections );
				$rows = self :: $db -> select( 'SELECT * FROM `look_lesson`
					WHERE `learner_id`=:lid AND `theme_id`=:id LIMIT 1',
					array(	'lid' => $sess[ 'id' ],
							'id' => $theme ) );
							
				$table = new Table( 'look_lesson' );
				if ( count( $rows ) ) {
					$look = end( $rows );
					$insert = $table -> getEntity( $look[ 'id' ] );
				}
				else {
					$insert = $table -> getEntity( );
				}

				$insert -> section_id	= $section[ 'id' ];
				$insert -> theme_id		= $theme;
				$insert -> learner_id	= $sess[ 'id' ];
				$insert -> datestamp	= time( );
				$insert -> ip 			= $_SERVER[ 'REMOTE_ADDR' ];
				$table -> save( $insert );
				return true;
			}
			return false;
		}


		// ������� �������� ����� ��������� �����
		public function getLookLesson( $theme ) {

			$auth = Auth :: getInstance( );
			$sess = $auth -> isAuth( );
			if ( $sess ) {
				$sections = self :: $db -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1',
						array( 'alias' => 'look_lesson' ) );
				if ( !count( $sections ) ) throw new Exception( 'section look_lesson not found' );
				$section = end( $sections );
				$rows = self :: $db -> select( 'SELECT * FROM `look_lesson`
					WHERE `learner_id`=:lid AND `theme_id`=:id LIMIT 1',
					array(	'lid' => $sess[ 'id' ],
							'id' => $theme ) );

				$table = new Table( 'look_lesson' );
				if ( count( $rows ) ) {
					$look = end( $rows );
					return $look;
				}
			}
			return array( );
		}

		// ���������� ������ �������� ����������
		public function getSessionActivity( $lid=null ) {
			$ret = array( );

			$activity = self :: $db -> select( 'SELECT `history_id`, `credit` FROM `position_learner_lesson`
				WHERE `learner_id`=:learner_id AND `credit`=1',
					array( 'learner_id' => $lid ) );
			foreach ( $activity as $a ) {
				$ret[ $a[ 'history_id' ] ] = $a[ 'credit' ];
			}
			return $ret;
		}
	
	
		// ������� ���������� ������ �� ����������
		public function getProgressNumber( $lid=null ) {

			$activity = self :: $db -> select( 'SELECT `t1`.`history_id`,
				`t2`.`id`,SUM(`t2`.`val`) as `summ` FROM `position_learner_lesson` `t1`
				LEFT JOIN `position_history` `t2` ON `t1`.`history_id`=`t2`.`id`
				WHERE `t1`.credit=1',
					array( 'learner_id' => $lid ) );

			return ( isset( $activity[ 0 ][ 'summ' ] ) ) ? $activity[ 0 ][ 'summ' ] : 0;
		}
	
	
	}





