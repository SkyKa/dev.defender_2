$(document).on('ready', function(){

	window.modal = Class.extend({

		init: function( config ) {

			this.template = {
				view: $('#modal-template').tmpl(),
				header: $('#modal-header-template').tmpl(),
				item: $('#modal-item-template').tmpl()
			};

			this.view( config );

		},

		uniq: function( config ) {

			var newArr = config.data.items.sort(function(a,b){return parseInt(a.parent_id) < parseInt(b.parent_id) ? -1 : 1;}).reduce(function(arr, el){
			    if(!arr.length || arr[arr.length - 1].parent_id != el.parent_id) {
			        arr.push(el);
			    }
			    return arr;
			}, []);

			var result = [];

			for ( var i = 0; i < newArr.length; i++ ) {
				result.push({
					theme: newArr[i].theme,
					parent_id: newArr[i].parent_id
				});
			}

			console.log(result)

			return result;

		},

		save: function( config ) {

			$.ajax({
				url: '/ajax/catalog.action.ajax_activity',
				dataType: 'json',
				data: {
					user_id: config.uid,
					history_id: config.hid,
					act: true
				}
			})
			.done(function(data){

				console.log(data)

				//$(this.viewHtml).modal('hide');
			});

		},

		concat: function( config ) {
			var result = [];

			for ( var i = 0; i < config.themes.length; i++ ) {
				var tmp = [];
				for ( var j = 0; j < config.data.items.length; j++ ) {
					if ( config.themes[i].parent_id == config.data.items[j].parent_id ) {
						tmp.push( config.data.items[j] );
					}

				}
				result.push( tmp );

			}

			return result;

		},

		view: function( config ) {	

			this.viewHtml = this.template.view( config.data );
			$( 'body' ).append( this.viewHtml );

			$(this.viewHtml).modal();

			var lessons = this.concat({
				themes: this.uniq( config ),
				data: config.data
			});

			for ( var i = 0; i < lessons.length; i++ ) {

				var theme = this.template.header({
					header: lessons[i][0].theme
				});

				$('.modal-body', this.viewHtml).append(theme);

				for ( var j = 0; j < lessons[i].length; j++ ) {
					
					var item = this.template.item({
						lesson: lessons[i][j],
						user_id: config.user_id
					});

					$(theme).append( item );

					$('input', $(item) ).on('change', function(e){

						var hid = $(e.delegateTarget).data('id');
						var uid = $(e.delegateTarget).data('user');
						this.save({
							uid: uid,
							hid: hid
						});
					}.bind(this))

				}

			}


			$(this.viewHtml).on('hidden.bs.modal', function(){
				this.remove();
			}.bind(this));

		},

		remove: function( config ) {

			$(this.viewHtml).remove();

		}

	});

	function addButton(){

		$('.position_learner .x-grid3-row-table').map(function(index,item){
			
			var id = $('tbody td:eq(1) div',item).html();
			$(item).append('<div data-id="' + id + '" class="edit_pensik">Вопросы</div>');

			$( '.edit_pensik', $(item) ).on('click', function(e){
				$.ajax({
					url:'/ajax/catalog.action.ajax_activity_cms',
					data:{
						user_id: id
					},
					dataType: 'json'
				})
				.done(function( data ) {

					new modal({
						data: data,
						user_id: id
					});

				});
			});

		});

	}

	$(document).on('click', $('#extdd-60').parent(), function(e){

		var t = setInterval(function(){

			if ($('#tab-catalog-section-edit-89 .x-grid3-row-table').length) {
				clearInterval(t);
				addButton();
			}

		}, 300);

	});
	
})